# Audio feature extractor
1 Feb 2017, 최근우

`feat_extract`의 후속 버전. 2017년 2월 베타 서비스에 사용될 32-dimensional audio feature


# 사용 방법
`main_feat_exact_jan2017.py` 참고.

실행은
``` bash
$ THEANO_FLAGS='device=gpu0' python main_feat_exact_jan2017.py
```

이며, theano와 tensorflow 모두 실행 가능하다. 단 `~/.keras/keras.json` 설정에서 `image_dim_ordering=th`로 해줘야 할 듯. `tf`로는 테스트 안해봄

# 결과

현재 3819357개의 유효한 `.npy`파일에서 피쳐를 추출했으며
* `/mnt/shared/storage4/audio_features`에
  - `audio_features.npy`
  - `audio_feature_track_ids.txt`

을 저장했다. npy 파일은 `(3819357, 32)`의 어레이이고 텍스트는 3819357 줄의 트랙 id이다

