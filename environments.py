import platform
import os
import sys

PATH_NPY_AUDIO = '/mnt/shared/storage5/npy-preview_16/'
PATH_A_FEAT = '/mnt/shared/storage4/audio_features/'
PATH_INVALID_NPY = 'invalid_npy'
SR = 12000
LEN_SRC = SR * 29

for folder in [PATH_A_FEAT, PATH_INVALID_NPY]:
    try:
        os.mkdir(folder)
    except:
        pass
