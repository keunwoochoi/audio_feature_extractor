from argparse import Namespace
from keras import backend as K
import os
from environments import *
import numpy as np
import models
import inputs
import pdb


class FeatureExtractor():
    def __init__(self, batch_size=64):
        self.model = None
        self.batch_size = batch_size
        self.reset_file()

    def load_model(self):
        last_layer = False
        args = Namespace(test=False, data_percent=100, model_name='', tf_type='melgram',
                         normalize='no', decibel=True, fmin=0.0, fmax=6000,
                         n_mels=96, trainable_fb=False, trainable_kernel=False)

        self.model = models.build_convnet_model(args=args, last_layer=last_layer)
        self.model.load_weights('weights_{}.hdf5'.format(K._backend),
                                by_name=True)

    def reset_file(self):
        self.track_ids = []

    def add_files_at(self, source_path):
        filenames = [f for f in os.listdir(source_path) if os.path.isfile(os.path.join(source_path, f)) and
                     f.split('.')[-1].lower() == 'npy']
        filenames.sort()
        self.track_ids += [f.replace('.npy', '') for f in filenames]

    def predict(self):
        if self.track_ids == []:
            return []
        x = []
        for tr_id in self.track_ids:
            subpath = inputs.track_id_to_path(tr_id)
            x.append(inputs._load_and_trim(os.path.join(PATH_NPY_AUDIO, subpath)))
        y = self.model.predict(np.array(x), batch_size=self.batch_size, verbose=1)

        return y
