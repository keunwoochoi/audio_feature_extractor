import numpy as np
from environments import *
import os
import string
import pdb
import sys



def write_txt(path, track_ids):
    with open(path, 'w') as f_w:
        f_w.write('\n'.join(track_ids))


def force_folder(folder_path, mode=None):
    absolute = folder_path.startswith('/')

    folders = folder_path.split('/')
    if '' in folders:
        folders.remove('')
    if absolute:
        fullpath = '/'
    else:
        fullpath = ''
    for folder in folders:
        fullpath = os.path.join(fullpath, folder)
        try:
            os.mkdir(fullpath)
        except:
            pass
        if mode is not None:
            try:
                os.chmod(fullpath, mode)
            except:
                pass


def path_iter(path_root):
    path_root = path_root.rstrip('/')
    numbers = [str(i) for i in range(10)]
    alphabets = list(string.ascii_lowercase)

    for sub1 in numbers:
        for sub2 in numbers + alphabets:
            for sub3 in numbers + alphabets:
                yield '%s/%s/%s/%s' % (path_root, sub1, sub2, sub3)


def track_id_to_path(track_id, ext='npy'):
    return '{}/{}/{}/{}.{}'.format(track_id[0],
                                   track_id[1].lower(),
                                   track_id[2].lower(),
                                   track_id,
                                   ext)


class LoadFailException(Exception):
    '''fail to load the numpy files. '''
    pass


def _load_and_trim(path):
    '''Load numpy file and make it (1, N) size
    '''
    raw = np.load(path)
    if raw.shape[0] > LEN_SRC:
        raw = raw[:LEN_SRC]
    elif raw.shape[0] < LEN_SRC:
        raw = np.concatenate((raw, np.zeros(LEN_SRC - raw.shape[0])))
    else:
        raw = raw
    # raw : (SR*29, )

    return raw[np.newaxis, :]


if __name__ == '__main__':
    '''Example'''
