import numpy as np
import feature_extractor
from environments import *
import inputs

def main():
    dim_feat = 32
    n_file = 3819357
    # Model
    fe = feature_extractor.FeatureExtractor()
    fe.load_model()
    track_ids = []
    audio_features = []
    # Data
    npy_paths = inputs.path_iter(PATH_NPY_AUDIO)
    for path_idx, npy_path in enumerate(npy_paths):
        fe.add_files_at(npy_path)

        # save it.
        if path_idx % 10 == 0:
            if fe.track_ids == []:
                continue
            feats = fe.predict()
            audio_features += [feat for feat in feats]
            track_ids += fe.track_ids
            fe.reset_file()
            if path_idx % 100 == 0:
                inputs.write_txt(os.path.join(PATH_A_FEAT, 'audio_feature_track_ids.txt'), track_ids)
                np.save(os.path.join(PATH_A_FEAT, 'audio_features.npy'), audio_features)
                print('Just saved the results at {}-th folder'.format(path_idx))
    else:
        if not fe.track_ids == []:
            # feats = fe.predict()
            # audio_features += [feat for feat in feats]
            fe.reset_file()

            inputs.write_txt(os.path.join(PATH_A_FEAT, 'audio_feature_track_ids.txt'), track_ids)
            np.save(os.path.join(PATH_A_FEAT, 'audio_features.npy'), audio_features)
            print('DONE. YAY!')

    # but only 3804374 files are converted.

if __name__ == '__main__':
    main()
